#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Este controller realiza ações sobre as tarefas,
isso inclui cadastrar, alterar, deletar, visualizar e
visualizar tudo
"""
from restAPI import app, db
from flask_restful import reqparse, Resource, Api
from restAPI.models.models_tarefas import TodoDatabase, todos_schema
from flask import jsonify

api = Api(app)

def serializer():
    """Serializa os dados que são recebidos como parâmetros

    utilizando o formado Json"""
    parser = reqparse.RequestParser()
    parser.add_argument('titulo', type=str)
    parser.add_argument('descricao', type=str)
    parser.add_argument('categoria', type=str)
    return parser.parse_args()


class Todo(Resource):
    """Métodos HTTP com necessidade da primary key como parâmetro"""
    def get(self, todo_id):
        """Retorna uma tarefa específica"""
        query = TodoDatabase.query.filter_by(id=todo_id)
        result = todos_schema.dump(query)
        return result.data


    def delete(self, todo_id):
        """Deleta uma tarefa específica"""
        TodoDatabase.query.filter_by(id=todo_id).delete()
        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)


    def put(self, todo_id):
        """Altera uma tarefa específica"""
        args = serializer()
        TodoDatabase.query.filter_by(id=todo_id).update({
            'titulo': args['titulo'].decode('utf-8'), 'descricao': args['descricao'].decode('utf-8'),
            'categoria': args['categoria'].decode('utf-8')})

        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)


class TodoList(Resource):
    """Métodos HTTP sem necessidade de primary keys """
    def get(self):
        """Retorna todas as tarefas"""
        query = TodoDatabase.query.all()
        result = todos_schema.dump(query)
        return result.data

    def post(self):
        """Insere nova tarefa"""
        args = serializer()
        db.session.add(TodoDatabase(
			None,
			args['titulo'].decode('utf-8'),
			args['descricao'].decode('utf-8'),
			args['categoria'].decode('utf-8'),
			))

        db.session.commit()
        result = {'Status': "Sucesso", 'HTTP': 201}
        return jsonify(result)

class TodosCategoria(Resource):
    """Métodos com relação direta as categorias"""
    def get(self, categoria):
        """Retorna todas as tarefas de uma determinada categoria"""
        query = TodoDatabase.query.filter_by(categoria=categoria)
        result = todos_schema.dump(query)
        return result.data


api.add_resource(TodoList, '/categorias/todos')
api.add_resource(TodosCategoria, '/categorias/<categoria>/todos')
api.add_resource(Todo, '/categorias/todos/<todo_id>')
